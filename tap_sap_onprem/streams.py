"""Stream type classes for tap-sap-onprem."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_sap_onprem.client import SapOnpremStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class ProductsStream(SapOnpremStream):
    """Define custom stream."""
    name = "products"
    path = "/ZPRODUCTS_SRV/ProductSet"
    records_jsonpath = "$.d.results[*]"
    primary_keys = ["ProductNo"]
    replication_key = "LastChangeDate"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("__metadata", th.ObjectType(
            th.Property('id',th.StringType),
            th.Property('uri',th.StringType),
            th.Property('type',th.StringType),
        )),
        th.Property('ProductNo',th.StringType),
        th.Property('Description',th.StringType),
        th.Property('CreatedOn',th.StringType),
        th.Property('CreatedBy',th.StringType),
        th.Property('LastChangeDate',th.DateTimeType),
        th.Property('ChangedBy',th.StringType),
        th.Property('ProductType',th.StringType),
        th.Property('DeletionFlag',th.BooleanType),
        th.Property('ProductGroup',th.StringType),
        th.Property('IndSector',th.StringType),
        th.Property('BaseUnit',th.StringType),
        th.Property('GrossWeight',th.StringType),
        th.Property('Status',th.StringType),
        th.Property('Message',th.StringType),
        th.Property('PriceSet',th.ObjectType(
            th.Property("results", th.CustomType({"type": ["array", "string"]})),
        )),
    ).to_dict()
class OrdersStream(SapOnpremStream):
    """Define custom stream."""
    name = "orders"
    path = "/ZORDERS_SRV/OrderSet"
    records_jsonpath = "$.d.results[*]"
    primary_keys = ["Reference"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("__metadata", th.ObjectType(
            th.Property('id',th.StringType),
            th.Property('uri',th.StringType),
            th.Property('type',th.StringType),
        )),
        th.Property('ProductNo',th.StringType),
        th.Property("Reference",th.StringType),
        th.Property("Tag",th.StringType),
        th.Property("Type",th.StringType),
        th.Property("PaymentStatus",th.StringType),
        th.Property("LeadTime",th.DateTimeType),
        th.Property("Meta",th.StringType),
        th.Property("Amount",th.StringType),
        th.Property("Customer",th.StringType),
        th.Property('ItemSet',th.ObjectType(
            th.Property("results", th.CustomType({"type": ["array", "string"]})),
        )),
    ).to_dict()

class PaymentStream(SapOnpremStream):
    """Define custom stream."""
    name = "payments"
    path = "/ZPAYMENTS_SRV/PaymentSet"
    records_jsonpath = "$.d.results[*]"
    primary_keys = ["Reference"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("__metadata", th.ObjectType(
            th.Property('id',th.StringType),
            th.Property('uri',th.StringType),
            th.Property('type',th.StringType),
        )),
        th.Property("Reference",th.StringType),
        th.Property("Payer",th.StringType),
        th.Property("Payee",th.StringType),
        th.Property("Amount",th.StringType),
        th.Property("Currency",th.StringType),
        th.Property("CreatedOn",th.StringType),
    ).to_dict()
class FulfilmentsStream(SapOnpremStream):
    """Define custom stream."""
    name = "fulfilments"
    path = "/ZFULFILLMENT_SRV/FulfillmentSet"
    records_jsonpath = "$.d.results[*]"
    primary_keys = ["Reference"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("__metadata", th.ObjectType(
            th.Property('id',th.StringType),
            th.Property('uri',th.StringType),
            th.Property('type',th.StringType),
        )),
        th.Property("Reference",th.StringType),
        th.Property("Tag",th.StringType),
        th.Property("LeadTime",th.DateTimeType),
        th.Property("Meta",th.StringType),
        th.Property("Amount",th.StringType),
        th.Property("FulfillmentItemSet", th.ObjectType(
            th.Property("results", th.CustomType({"type": ["array", "string"]})),
        )),
    ).to_dict()